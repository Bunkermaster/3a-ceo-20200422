<?php
require 'pages.php';
if (!isset($_GET['page'])) {
    $pageId = 'index';
} else {
    $pageId = $_GET['page'];
}
if (!isset($page[$pageId])) {
    header('Location: index.php');
    exit;
}
$pageContent = $page[$pageId];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?=$pageContent['title']?></title>
</head>
<body>
<?php require "nav.php"?>
<h1><?=$pageContent['h1']?></h1>
<img src="<?=$pageContent['image']?>" alt="">
</body>
</html>
