<?php
/*
 * Page:
 * - title
 * - h1
 * - image
 */
$page = [];
$page['index'] = [
    'title' => 'Chatonton',
    'h1'    => 'Kitten!',
    'image' => 'img/Unknown.jpg',
];
$page['teletubbies'] = [
    'title' => 'Teletubbies',
    'h1'    => 'Teletubbies',
    'image' => 'img/teletubbies.jpg',
];
$page['lorenzo'] = [
    'title' => 'Lolo',
    'h1'    => 'Lolo',
    'image' => 'img/lolo2.jpg',
];
$page['bobleponge'] = [
    'title' => 'Bob!!',
    'h1'    => 'Une eponge carré, lul!',
    'image' => 'img/bob.png',
];
$page['sailormoon'] = [
    'title' => 'Pewpew!!',
    'h1'    => 'Saloirmoon',
    'image' => 'img/sailor-moon.jpg',
];
